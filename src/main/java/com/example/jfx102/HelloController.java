package com.example.jfx102;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class HelloController {

    public static int n_pressed = 1;

    @FXML
    private Label commentLabel;

    @FXML
    private Label fahrenheitLabel;
    @FXML
    private TextField celsiusTextField;

    @FXML
    protected void onHelloButtonClick() {
        double fahrenheit = 0.0;
        commentLabel.setText("Hi! Button pressed " + n_pressed + " Times");
        try {
            fahrenheit = Double.parseDouble(celsiusTextField.getText()) * (9.0/5.0) + 32.0;   // T(oF) = (T(oC) × (9/5)) + 32
            fahrenheitLabel.setText(Double.toString(fahrenheit));
        } catch (NumberFormatException e) {
            System.out.println("Floating Point Number Only!");
            fahrenheitLabel.setText("Floating Point Number Only!");
        } catch (Exception e) {
            System.out.println(e);
        }
        n_pressed++;
    }
}