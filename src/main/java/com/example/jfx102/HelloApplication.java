package com.example.jfx102;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

// Made For Software Design Course at FEUP 2022
// Contact author:  asousa@fe.up.pt
// A mixture of the IntelliJ default project with JavaFx
// with the video https://youtu.be/As7TEjqJ3Ao?list=PLZPZq0r_RZOM-8vJA3NQFZB7JroDcMwev



public class HelloApplication extends Application {
    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource("hello-view.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 320, 240);
        stage.setTitle("Hello Software Design!");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}